import torch
import torch.nn as nn
import torch.nn.functional as F

class legoNet(nn.Module):
    def __init__(self, IMG_SIZE):
        super().__init__()
        self.conv1 = nn.Conv2d(1, 32, 5)
        self.conv2 = nn.Conv2d(32, 64, 5)
        self.conv3 = nn.Conv2d(64, 128, 5)

        X = torch.randn(IMG_SIZE, IMG_SIZE).view(-1, 1, IMG_SIZE, IMG_SIZE)
        self.toLinear = None
        self.convs(X)

        self.fc1 = nn.Linear(self.toLinear, 1024)
        self.fc2 = nn.Linear(1024, 512)
        self.fc3 = nn.Linear(512, 20)

    def convs(self, x):
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))

        if self.toLinear == None:
            self.toLinear = len(torch.flatten(x[0]))
            #print(f"toLinear: {self.toLinear}")
        return x
    
    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self.toLinear)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return F.softmax(x, dim=1)
    