import glob
import os
import shutil 
import re
from tqdm import tqdm

class ImageCountBalancer:

    def __init__(self, image_dir_path, delete_step, duplication_step, balance_amount):
        self.image_dir_path = image_dir_path
        self.delete_step = delete_step
        self.duplication_step = delete_step

        if not balance_amount:
            balance_amount = self.get_average() 

        self.balance_image_count(balance_amount)

    def image_count_above_balance_amount(self, balance_amount, image_folder):
        image_count = len(glob.glob(image_folder + "/*"))
        if image_count == balance_amount:
            pass

        return image_count > balance_amount

    def get_image_count(self, path):
        return len(glob.glob(path + "/*"))

    def get_average(self):
        image_folders = glob.glob(self.image_dir_path)
        total_image_folders = len(image_folders)
        if not image_folders:
            print("No files found at this path")
            return False
        amount_of_image = 0

        for image_folder in tqdm(image_folders, desc="Getting average count"):
            amount_of_image += len(glob.glob(image_folder + "/*"))

        average_image = int(amount_of_image / total_image_folders)
        return average_image       
        
    
    def remove_extra_image_in_folder(self, image_folder, balance_amount):
        folder_image = glob.glob(image_folder + "/*")
        for idx, image in enumerate(folder_image):
            image_count = self.get_image_count(image_folder)
            if image_count <= balance_amount:
                    break
            if idx % self.delete_step == 0:
                os.remove(image)
            image_count = self.get_image_count(image_folder)
        if image_count > balance_amount:
            self.remove_extra_image_in_folder(image_folder, balance_amount)
        else:
            return True;
   
    def duplicate_images_in_folder(self, image_folder, balance_amount):
        folder_image = glob.glob(image_folder + "/*")
        for idx, image in enumerate(folder_image): 
            image_count = self.get_image_count(image_folder)  
            if image_count >= balance_amount:
                break
            if idx % self.delete_step == 0:
                replace = "duplicated{}.jpg".format(idx)
                target = re.sub('(\.jpg)', replace, image)
                shutil.copyfile(image, target)
            image_count = self.get_image_count(image_folder)
        if image_count < balance_amount:
            self.duplicate_images_in_folder(image_folder, balance_amount)
        else:
            return True;

    def balance_image_count(self, balance_amount):
        if not balance_amount:
            return False

        images_folders = glob.glob(self.image_dir_path)       
        for image_folder in tqdm(images_folders, desc="Balancing images"):
            remove_images = self.image_count_above_balance_amount(balance_amount, image_folder)
            if remove_images:
                self.remove_extra_image_in_folder(image_folder, balance_amount)
            else :
                self.duplicate_images_in_folder(image_folder, balance_amount)
      
        print("Success! All folders now have {} Images".format(balance_amount))
