import numpy as np
import torch
import matplotlib.pyplot as plt
import legoNN as lgn
from tqdm import tqdm
import torch.optim as opt
import torch.nn as nn
import time

class NetTrainer:
    def __init__(self, device, imgSize, data, dataTestPercentage, networkName, logPath):
        self.device = device
        self.imgSize = imgSize
        self.data = data
        self.networkName = networkName
        self.logPath = logPath
        self.testPCT = dataTestPercentage

        self.net = lgn.legoNet(self.imgSize).to(device=self.device)
        
    
    def prepData(self):
        trainingData = np.load(self.data, allow_pickle=True)
        print(f"loaded formatted data, size: {len(trainingData)}")

        SAMPLE = torch.Tensor([e[0] for e in trainingData]).view(-1, self.imgSize, self.imgSize)
        SAMPLE = SAMPLE/255.0
        LABEL = torch.Tensor([e[1] for e in trainingData])

        SAMPLE, LABEL = SAMPLE.to(self.device), LABEL.to(self.device)


        valSize = int(len(SAMPLE)*self.testPCT)
        print(f"test data size: {valSize}")

        TEST_SAMPLE = SAMPLE[-valSize:]
        TEST_LABEL = LABEL[-valSize:]

        SAMPLE = SAMPLE[:-valSize]
        LABEL = LABEL[:-valSize]
        print(f"train data size: {len(SAMPLE)}")
        
        return (SAMPLE, LABEL, TEST_SAMPLE, TEST_LABEL)
    
    def trainNet(self, epochs, batchSize, showExample=False):
        SAMPLE, LABEL, TEST_SAMPLE, TEST_LABEL = self.prepData()
        
        if showExample == True:
            print(LABEL[0].cpu())
            plt.imshow(SAMPLE[0].cpu(), cmap='gray')
            plt.show()
        
        optimiser = opt.Adam(self.net.parameters(), lr=0.001)
        loss_func = nn.MSELoss()
        
        with open(self.logPath, "w+") as sf:
            sf.write("epoch,trainLoss,testLoss,trainAccuracy,testAccuracy\n")
            for epoch in range(epochs):
                #train net
                for i in tqdm(range(0, len(LABEL), batchSize), desc=f"epoch: {epoch}"):
                    sample_batch = SAMPLE[i:i+batchSize].view(-1, 1, self.imgSize, self.imgSize)
                    label_batch = LABEL[i:i+batchSize]

                    self.net.zero_grad()
                    outputs = self.net(sample_batch)
                    loss = loss_func(outputs, label_batch)
                    loss.backward()
                    optimiser.step()
                

                #check loss against validation data, but don't train against validation data
                for i in tqdm(range(0, len(TEST_SAMPLE), batchSize), desc="Loss testing"):
                    testSamples = TEST_SAMPLE[i:i+batchSize].view(-1, 1, self.imgSize, self.imgSize)
                    testLabels = TEST_LABEL[i:i+batchSize]

                    self.net.zero_grad()
                    testOut = self.net(testSamples)
                    testLoss = loss_func(testOut, testLabels)
                
                acc = self.testNet(TEST_SAMPLE, TEST_LABEL, SAMPLE, LABEL)
                sf.write(f"{epoch},{loss},{testLoss},{acc[0]},{acc[1]}\n")

                
                print(f"Epoch: {epoch}. Loss: {loss}. Accuracy: {acc[0]}")

        torch.save(self.net.state_dict(), self.networkName)
        
        
    def testNet(self, tSample, tLabel, sample, label):
        return [self.getNetAccuracy(sample, label), self.getNetAccuracy(tSample, tLabel)]


    def getNetAccuracy(self, sample, label):
        total = 0
        correct = 0
        with torch.no_grad():
            for i in tqdm(range(len(sample)), desc="Accuracy testing"):
                expected = torch.argmax(label[i])
                output = self.net(sample[i].view(-1, 1, self.imgSize, self.imgSize))
                prediction = torch.argmax(output)
            
                if prediction == expected:
                    correct += 1
                total += 1
        
        print(f"correct: {correct} out of: {total}")
        return (correct/total)