# lego-ir

A lego image recognition ai

## description & usage

This ir (image recognition) ai is a model that can differentiate between the different types of lego blocks. This ai was mode as a replacement assignment for a low code module.  
The code may be freely used and distributed but only with the permission of contributors.  
For more information check the included license.

## Setup your Python virtual environments

Create a new Python virtual environment: `python -m venv _env`  
  
Switch to the new Python virtual environment:  
Mac OS / Linux: `source _env/bin/activate`  
Windows: `_env\Scripts\activate`  

Check if the new Python virtual environment is active `pip -V`  

Upgrade pip in the virtual environment: `pip install --upgrade pip`  
Install the needed dependencies: `pip install -r requirements.txt`  

For more information [see this page](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/)  

## Pip tips
Remove all pip packages: `python -m venv --clear _env`  
Save all packages to the `requirements.txt`: `pip freeze > requirements.txt`  
On Macos/Linux remove all pip packages: `pip freeze | xargs pip uninstall -y`  

## Configuring the dataset
The dataset is located in the dataset folder and stored in a zip file
You can extract the zip file in the dataset folder the train the ai with

## Data options
In order to train the model and use this network you can configure some variables

To resort the data set `RESORT_DATA` to True
To rebuild new data set `REBUILD_DATA` to True 
To retrain the network set `RETRAIN_NET` to True
To view the logs set set `VIEW_LOG` to True

## User options 
If you want to view historical records set `VIEW_RECORD` to True
To make a prediction over an imgage set `INPUT_IMAGE_PATH` to the path of the image 
If you want to see an example of the dataset, set `SHOW_EXAMPLE` to True

## Network settings
To set the epoch count set `EPOCH` to a number
To set the image width and height size set `IMG_SIZE` to a number
To set the data test percentage set `DATA_TEST_PERCENTAGE` number
To set the batch size set `BATCH_SIZE` to a number

## Training and using the model
In order to use and train the model you need the following folders:
- dataset
- trainLogs
- networks