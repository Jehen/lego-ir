import torch
import ImageCountBalancer as icbm
import Dataloader as dtl
import netTrainer as ntt
import pandas as ps
import matplotlib.pyplot as plt
import legoNN as lgn
import os
import cv2

def displayLog(LOG_PATH):
    log = ps.read_csv(LOG_PATH)
    log.plot(x="epoch", y=["trainLoss", "testLoss", "trainAccuracy", "testAccuracy"], color=["red", "orange", "blue", "green"], xticks=range(EPOCH))
    plt.title("model fitness by loss and epoch")
    plt.show()

if __name__ == "__main__":
    print("hello world")
    
    #Comment out if your laptop has little VRAM, it may crash otherwise
    device = torch.device("cpu")
    if torch.cuda.is_available():
       device = torch.device("cuda:0")

    #data options
    RESORT_DATA = True
    REBUILD_DATA = True
    RETRAIN_NET = True
    VIEW_LOG = True
    
    #user options
    VIEW_RECORD = None
    INPUT_IMAGE_PATH = None #"./dataset/cropped_images/Brick_1x3/1_Brick_1x3_180709172316.jpg"
    SHOW_EXAMPLE = False

    #data vars
    BALANCING_RESOLUTION = 10
    BALANCE_POINT = None
    
    #network and training vars
    EPOCH = 10
    IMG_SIZE = 50
    DATA_TEST_PERCENTAGE = 0.1
    BATCH_SIZE = 100

    #network and data savefiles
    CLASSIFICATION_FILE = "./keyClassification.csv"
    DATASET_PATH = "./dataset/archivecropped_images/*"
    FORMATTED_DATA = "./formattedData/lol.npy"
    NET_NAME = "./networks/legoNet"
    LOG_PATH = f"./trainLogs/lgEpochs{EPOCH}.csv"
    
    
    if VIEW_RECORD or INPUT_IMAGE_PATH:
        if VIEW_RECORD:
            displayLog(VIEW_RECORD)
        
        if INPUT_IMAGE_PATH:
            try:
                img = None

                absolatedPath = os.path.abspath(INPUT_IMAGE_PATH)
                img = cv2.imread(absolatedPath, cv2.IMREAD_GRAYSCALE)
                img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
                
                sample = torch.Tensor(img).view(-1, IMG_SIZE, IMG_SIZE)
                sample = sample/255.0

                sample = sample.to(device=device)

                net = lgn.legoNet(IMG_SIZE).to(device=device)
                net.load_state_dict(torch.load(NET_NAME))
                net.eval()

                with torch.no_grad():
                    output = net(sample.view(-1, 1, IMG_SIZE, IMG_SIZE))
                    prediction = torch.argmax(output)

                res = prediction.to("cpu").numpy()

                classification = ps.read_csv(CLASSIFICATION_FILE)
                print(f"prediction for your image: {classification['directory'].loc[(classification['classification'] == res)].item()}")

            except Exception as e:

                print(str(e))
                
    else:

        if RESORT_DATA:
            icbm.ImageCountBalancer(DATASET_PATH, BALANCING_RESOLUTION, BALANCING_RESOLUTION, BALANCE_POINT)
        
        if REBUILD_DATA or RESORT_DATA:
            dl = dtl.Dataloader(IMG_SIZE, DATASET_PATH, FORMATTED_DATA, CLASSIFICATION_FILE)
            dl.formatDataSet()
            
        if RETRAIN_NET:
            trainer = ntt.NetTrainer(device, IMG_SIZE, FORMATTED_DATA, DATA_TEST_PERCENTAGE, NET_NAME, LOG_PATH)
            trainer.trainNet(EPOCH, BATCH_SIZE, SHOW_EXAMPLE)
        
        if VIEW_LOG: 
            displayLog(LOG_PATH)
    
    print("goodbye world")