import os
import cv2
import numpy as np
from tqdm import tqdm
import csv
import pandas as ps
import shutil

class Dataloader:
    def __init__(self, imgSize, sourcePath, dataDestination, classificationFile):
        self.imgSize = imgSize
        self.path = sourcePath.replace("*","")
        self.destination = dataDestination
        
        self.classification = ps.read_csv(classificationFile)
        
        self.processed_data = []
    
    def readClassification(self, directoryName):
        return self.classification["classification"].loc[(self.classification["directory"] == directoryName)].item()

    def formatDataSet(self):    
        err = 0
        
        for d in tqdm(os.listdir(self.path), desc="Creating dataset"):
            classification = self.readClassification(d)
            
            for f in tqdm(os.listdir(f"{self.path}/{d}"), desc=f"Processing {d}"):
                if "jpg" in f:
                    try:
                        img = cv2.imread(f"{self.path}/{d}/{f}", cv2.IMREAD_GRAYSCALE)
                        img = cv2.resize(img, (self.imgSize, self.imgSize))
                        self.processed_data.append([np.array(img), np.eye(20)[classification]])

                    except Exception as e:
                        print(f"{self.path}/{d}/{f}", str(e))
                        err += 1
        
        print(f"Errors processing training data: {err}")
        
        np.random.shuffle(self.processed_data)
        np.save(self.destination, self.processed_data)
        self.processed_data = []
        
        print("Successfully processed all data!")